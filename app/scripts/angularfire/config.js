angular.module('firebase.config', [])
  .constant('FBURL', 'https://fiery-inferno-4482.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['anonymous','facebook','google','twitter','github'])

  .constant('loginRedirectPath', '/login');
