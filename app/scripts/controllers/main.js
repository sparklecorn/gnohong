'use strict';

/**
 * @ngdoc function
 * @name gnohongApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gnohongApp
 */
angular.module('gnohongApp')
  .controller('MainCtrl', function ($scope, Ref, $firebaseObject) {
    var info = $firebaseObject(Ref.child('info'));

    console.log('pre-load info:', info);
    info.$loaded().then(function() {
      console.log('post-load info:', info);
    });

    $scope.info = info;

    /*
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var introductions = [
      'who am i',
      'going forward heading back'
    ];

    var randomIdx = Math.floor((Math.random() * 2) + 0);
    var stop;
    
    $scope.intro = 2;

    $scope.forwardIntro = function() {
      return introductions[randomIdx];
    };

    $scope.reversedIntro = function() {
      var txt = $scope.forwardIntro();

      var reversedIntro = (function() {
        var length = txt.length;
        var temp = [];
        for(var i=length-1; i >= 0; i--){
          temp.push(txt[i]);
        }
        return temp.join('');
      })();

      return reversedIntro;
    };

    stop = $timeout(function() {
      $scope.intro = 1;
    }, 3000);
    */

  });
