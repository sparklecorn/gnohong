'use strict';

/**
 * @ngdoc overview
 * @name gnohongApp
 * @description
 * # gnohongApp
 *
 * Main module of the application.
 */
angular.module('gnohongApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.ref',
    'firebase.auth'
  ]);
